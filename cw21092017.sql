﻿CREATE TABLE employee(
id integer NOT NULL PRIMARY KEY,
name text,
salaray int,
 manager_id int,
department_id int
);
insert into employee(id, name, salary) values(1, 'Вася', 10), (2, 'Петя', 15);
insert into employee(id, name, salary) values(3, 'Костя', 30), (4, 'Никита', 3);
update employee set salary = 20 where name = 'Вася';
delete from employee where name = 'Петя';
select * from employee;
insert into employee(id, name, salary) values(5, 'Кок', 10), (6, 'Петя', 35);
insert into employee(id, name, salary) values(7, 'Костя', 22), (8, 'Никита', 31);
update employee set manager_id = 1;
update employee set department_id = 1 where name in('Вася','Петя','Костя');
update employee set department_id = 2 where name in('Никита','Кок','Петя','Костя','Никита');
select department_id, max(salary) as salary from employee group by department_id;
select department_id, avg(salary) as salary from employee  group by department_id having AVG(salary) > 10;
alter table employee add constraint manager_employee_fk foreign key(manager_id) references employee(id);
update employee set manager_id = null;
select * from employee;
update employee set manager_id = 8 where name = 'Кок';
select e.name, e.salary, man.salary from employee as e join employee as man on e.manager_id = man.id where
e.salary > man.salary 
select e.name from  employee e 
where e.salary = (select max(salary) from employee e2 
where e2.department_id = e.department_id)